class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        total_price = 0
        for product in self.products:
            total_price += product.get_price()
        return total_price

    def get_total_quantity_of_products(self):
        total_quantity = 0
        for product in self.products:
            total_quantity += product.quantity
        return total_quantity

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


if __name__ == '__main__':
    # Testy
    order1 = Order('adrian@example.com')
    print(order1.customer_email)

    shoes = Product('Shoes', 30.00, 3.0)
    tshirt = Product('T-Shirt', 50.00, 2.0)
    bag = Product('Bag', 10.00)

    order1.add_product(shoes)
    order1.add_product(tshirt)
    order1.add_product(bag)

    print(len(order1.products))
    print("Price of the first product:", order1.products[0].get_price())
    print("Total price:", order1.get_total_price())
    print("Total quantity of products:", order1.get_total_quantity_of_products())


